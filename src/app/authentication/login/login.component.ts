import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginFailed: boolean;
  form: FormGroup;
  constructor(private autService: AuthService) { }

  ngOnInit() {
    this.loginFailed = this.autService.getisAuthenticated();

    this.form = new FormGroup({
      'email': new FormControl(null, {validators: [Validators.required, Validators.email]}),
      'password': new FormControl(null, {validators: [Validators.required, Validators.minLength(3)]})
    });

    this.autService.getLoginFailedListener().subscribe(loginFailed =>  {
      this.loginFailed = loginFailed;
    });
  }

  onLogin() {
    if (!this.form.valid) {
      console.log('form invalid');
      return;
    }
    this.autService.loginUser(this.form.value.email, this.form.value.password);
  }
}

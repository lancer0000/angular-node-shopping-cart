import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({providedIn: 'root'})
export class AuthService {
  private tokenTimer: any;
  isAuthenticated = false;
  token: string;
  isLoggedIn = new Subject<boolean>();
  loginFailed = new Subject<boolean>();
  signupFailed = new Subject<boolean>();
  constructor(private http: HttpClient, private router: Router) {}

  createUser(email: string, password: string) {
   this.http.post<any>('http://localhost:3000/signupuser', {email: email, password: password}).subscribe(
     res => {
       if (!res.err) {
        console.log(res.message);
        this.loginUser(email, password);
        this.router.navigate(['/shop']);
       } else {
        console.log('res.error', res.err);
       }
     }, (error) => {
       this.signupFailed.next(true);
       console.log('error:', error);
       // hide the error messsage after 10seconds
       setTimeout(() => {
         this.signupFailed.next(false);
       }, 10000);
     }
   );
  }

  loginUser(email: string, password: string) {
    this.http.post<any>('http://localhost:3000/loginuser', {email: email, password: password}).subscribe(
      response => {
        const token = response.token;
        this.token = token;
        console.log(response);
        this.setAuthenticationTimer(response.expiresIn);
        const expirationDate = new Date(new Date().getTime() + response.expiresIn * 1000);
        this.saveAuthData(token, expirationDate);
        console.log(expirationDate);

        this.isLoggedIn.next(true);
        this.isAuthenticated = true;
        this.router.navigate(['/shop']);
      }, err => {
        this.loginFailed.next(true);
        // hide the error messsage after 10seconds
        setTimeout(() => {
          this.loginFailed.next(false);
        }, 10000);
      }
    );
  }

  logoutUser() {
    this.token = null;
    this.isLoggedIn.next(false);
    this.isAuthenticated = false;
    console.log('User was logged out successfully!');
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(['/']);
  }

  autoAuthenticateUser() {
    const authInformation = this.getAuthenticationData();
    const now = new Date();
    // authInformation.expirationDate is a string hence wwhy we need to create a date off of it with new Date
    const expiresIn = new Date(authInformation.expirationDate).getTime() - now.getTime();
    if (expiresIn > 0) {
      this.token = authInformation.token;
      this.isLoggedIn.next(true);
      this.isAuthenticated = true;
      this.setAuthenticationTimer(expiresIn / 1000);
    }
  }

  getAuthenticationData() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    if (!token || !expirationDate) {
      return;
    }
    return {
      token,
      expirationDate
    };
  }

  private setAuthenticationTimer(duration: number) {
    console.log('Setting timer...', duration);
    this.tokenTimer = setTimeout(() => {
      this.logoutUser();
    }, duration * 1000);
  }

  getLoginFailedListener() {
    return this.loginFailed.asObservable();
  }

  getisLoggedInListener() {
    return this.isLoggedIn.asObservable();
  }

  getisAuthenticated() {
    return this.isAuthenticated;
  }

  getsignupFailedListener() {
    return this.signupFailed.asObservable();
  }

  getToken() {
      return this.token;
  }

  private saveAuthData(token: string, expirationDate: Date) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
  }

  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
  }
}

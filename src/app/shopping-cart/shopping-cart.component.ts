import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { ShoppingCartService } from './shopping-cart.service';
import { Subscription } from 'rxjs';
import { PaymentsService } from '../payment/payments/payments.service';
import {environment} from '../../environments/environment';


@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit, OnDestroy {
  // shopping cart properties
  cartItems = [];
  totalProductsPrice = 0;
  subscription: Subscription;
  // make payments properties
  handler: any;

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit() {
    this.shoppingCartService.getShoppingCartItems();
    this.subscription = this.shoppingCartService.getProductsChangedListener().subscribe(products => {
      // reset the counter each time we get a now products array
      this.totalProductsPrice = 0;
      this.cartItems = products;
      const priceArray = this.cartItems.map(cartItem => cartItem.price * cartItem.count);
      for (const price of priceArray) {
        this.totalProductsPrice = this.totalProductsPrice + price;
      }
    });
    // Stripe Checkout config
    this.handler = StripeCheckout.configure({
      key: environment.stripeKey,
      image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
      locale: 'auto',
      token: token => {
        this.shoppingCartService.processPayment(token, this.totalProductsPrice * 100);
      }
    });
  }

  onReduceBy1(product) {
    this.shoppingCartService.removeFromShoppingCart(product);
  }
  onRemoveAll(product) {
    this.shoppingCartService.removeAllFromShoppingCart(product);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // payments functions
  handlePayment() {
    this.handler.open({
      name: 'Checkout',
      description: 'Deposit Funds to Account',
      amount: this.totalProductsPrice * 100
    });
  }

  @HostListener('window:popstate')
  onpopstate() {
    this.handler.close();
  }
}

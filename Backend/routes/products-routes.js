const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const stripe = require("stripe")("sk_test_SXbTcADp3k1PF6vHIwT7oVPW");
const jwt = require('jsonwebtoken');

const checkAuth = require('../middlewares/Authorization');

const config = {
  host: 'localhost',
  user: 'root',
  // insecureAuth: true,
  password: 'sousouke99',
  // port: '3000',
  database: 'shoppingcartdb',
  multipleStatements: true
};


//promisifying the DB
class Database {
  constructor( config ) {
      this.connection = mysql.createConnection( config );
  }
  async query( sql, args ) {
      return new Promise( ( resolve, reject ) => {
          this.connection.query( sql, args, ( err, rows ) => {
              if ( err )
                  return reject( err );
              resolve( rows );
          } );
      } );
  }
  close() {
      return new Promise( ( resolve, reject ) => {
          this.connection.end( err => {
              if ( err )
                  return reject( err );
              resolve();
          } );
      } );
  }
};

let database = new Database(config);

//routes
//get user's saved products to shopping cart
router.get('/shoppingcart', checkAuth, async (req, res, next) => {
  const token = req.headers.authorization.split(" ")[1];
  const userID = jwt.decode(token).userId;
  try {
    let rows = await database.query(`SELECT * FROM products WHERE userID=${userID}`);
    if (!rows) {
      return res.status(500).json({
        message: 'Internal server error occured, try again later',
        error: err
      });
    }

    res.status(200).json({
      message: 'User\'s products fetched successfully!',
      products: JSON.parse(JSON.stringify(rows))
    });
  } catch (e) {
    res.status(500).json({
      message: 'Internal server error occured, try again later',
      error: err
    });
  }
});


router.post('/shopping-cart', checkAuth, async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const userID = jwt.decode(token).userId;
    let check = await database.query(`SELECT * FROM products WHERE name='${req.body.name}' AND userID=${userID}`);
    if (check.length === 0) {
      const product = {...req.body, userID: userID};
      let result = await database.query('INSERT INTO products SET ?', product);
      //result.insertId returns the inserted row's id
      // await connection.query(`INSERT INTO userproduct (userID, prodID) VALUES (${userID}, ${result.insertId})`);
    } else {
      await database.query(`UPDATE products SET count = count + 1 WHERE name='${req.body.name}' AND userID=${userID}`);
    }

    res.status(201).json({
      message: 'Product added!',
      error: null
    });
  } catch(e) {
    console.log(e);
    res.status(400).json({
      message: 'Couldn\'t add product to shopping cart.',
      error: e
    });
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (req.body.count > 1) {
      await database.query(`UPDATE products SET count = count - 1 WHERE prodID=${req.body.prodID}`);
      res.status(200).json({
        message: 'Resource was updated successfully!'
      });
    } else {
      await database.query(`DELETE FROM products WHERE prodID=${req.body.prodID}`);
      res.status(200).json({
        message: 'Product deleted!'
      });
    }
  } catch (e) {
    res.status(500).json({
      message: 'An error occured when deleting product. Server error, try again later.'
    });
  }
});

router.put('/', async (req, res, next) => {
  try {
    await database.query(`DELETE FROM products WHERE prodID=${req.body.prodID}`);
    res.status(200).json({
      message: 'Product deleted!'
    });
  } catch(e) {
    res.status(500).json({
      message: 'An error occured when deleting product. Server error, try again later.'
    });
  }
});

// payment route
router.post('/shoppingcart/checkout', async (req, res, next) => {
  try {
        const tokenId = req.body.token.id;
        const cardId = req.body.token.card.id;
        const amount = req.body.amount;

        const customer = await stripe.customers.create({
          source: tokenId,
          email: req.body.token.email
        });
        // charge the customer instead of the card
        const charge = await stripe.charges.create({
          amount: amount,
          currency: 'usd',
          description: 'test charge',
          customer: customer.id
        });

  } catch (e) {
    res.status(500).json({
      message: 'Purchase failed, Server error!'
    });
  }
});


module.exports = router;
